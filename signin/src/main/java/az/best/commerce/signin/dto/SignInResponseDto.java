package az.best.commerce.signin.dto;

public class SignInResponseDto {
    private final String jwt;

    public SignInResponseDto(String jwt) {
        this.jwt = jwt;
    }

    public String getJwt() {
        return jwt;
    }
}
