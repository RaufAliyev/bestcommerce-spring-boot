package az.best.commerce.signin.service;

import az.best.commerce.signin.dto.SignInResponseDto;
import az.best.commerce.signin.exception.UserNotFoundException;
import az.best.commerce.signin.util.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
public class SignInService {

    private AuthenticationManager authenticationManager;
    private MyUserDetailsService userDetailsService;
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    public SignInService(AuthenticationManager authenticationManager, MyUserDetailsService userDetailsService, JwtTokenUtil jwtTokenUtil) {
        this.authenticationManager = authenticationManager;
        this.userDetailsService = userDetailsService;
        this.jwtTokenUtil = jwtTokenUtil;
    }

    public SignInResponseDto checkUserAndCreateToken(String email, String password){
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        }catch (BadCredentialsException e){
            throw new UserNotFoundException("Email or Password is incorrect");
        }

        final UserDetails userDetails = userDetailsService.loadUserByUsername(email);
        final String jwt = jwtTokenUtil.generateToken(userDetails);
        return new SignInResponseDto(jwt);
    }

}
