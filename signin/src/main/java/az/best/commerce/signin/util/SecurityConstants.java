package az.best.commerce.signin.util;

public class SecurityConstants {
    public static final long EXPIRATION_TIME = 43_200_000; // 12 hours
    public static final int REMEMBER_ME_VALID_SECOND = 604800;
    public static final String SIGN_IN_URL = "/api/bestcommerce/sign-in";
}
