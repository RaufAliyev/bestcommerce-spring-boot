package az.best.commerce.signin.exception;

public class UserNotFoundException extends CustomBaseException {

    public UserNotFoundException(String message) {
        super(message);
    }
}
