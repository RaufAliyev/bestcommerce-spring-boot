package az.best.commerce.signin.service;

import az.best.commerce.signin.entity.Merchant;
import az.best.commerce.signin.repository.SignInRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class MyUserDetailsService implements UserDetailsService {

    private SignInRepository signInRepository;

    @Autowired
    public MyUserDetailsService(SignInRepository signInRepository) {
        this.signInRepository = signInRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Merchant merchant = signInRepository.findByEmailAddress(email).
                orElseThrow(() -> new UsernameNotFoundException("User Not Found with email: " + email));

       return new User(merchant.getEmailAddress(), merchant.getPassword(), new ArrayList<>());
    }
}
