package az.best.commerce.signin.controller;

import az.best.commerce.signin.dto.ServiceResponse;
import az.best.commerce.signin.dto.SignInRequestDto;
import az.best.commerce.signin.dto.SignInResponseDto;
import az.best.commerce.signin.service.SignInService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/bestcommerce")
public class SignInController {

    private final Logger log = LogManager.getLogger(SignInController.class);

    private SignInService signInService;

    @Autowired
    public SignInController(SignInService signInService) {
        this.signInService = signInService;
    }

    @PostMapping(value = "/sign-in")
    public ResponseEntity<?> addMerchant(@Valid @RequestBody SignInRequestDto requestDto){
        log.info(String.format("[Request]  %s", requestDto.toString()));
        SignInResponseDto  signInResponseDto = signInService.checkUserAndCreateToken(requestDto.getEmail(),requestDto.getPassword());
        log.info(String.format("[Response]  %s", requestDto.toString()));
        return ResponseEntity.ok(ServiceResponse.of(true,null,signInResponseDto));
    }
}
