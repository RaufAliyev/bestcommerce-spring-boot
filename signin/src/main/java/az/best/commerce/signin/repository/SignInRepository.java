package az.best.commerce.signin.repository;

import az.best.commerce.signin.entity.Merchant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SignInRepository extends JpaRepository<Merchant, Long> {
    Optional<Merchant> findByEmailAddress(String email);
}
