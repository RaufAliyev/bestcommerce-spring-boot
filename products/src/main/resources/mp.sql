INSERT INTO merchant_product(id,merchant_id,product_id,unit_price,discount_price,inventory,payment_option)
VALUES(1,1,1,2000,0,15,'Installments');
INSERT INTO merchant_product(id,merchant_id,product_id,unit_price,discount_price,inventory,payment_option)
VALUES(2,1, 2,800,0,10,'Direct');
INSERT INTO merchant_product(id,merchant_id,product_id,unit_price,discount_price,inventory,payment_option)
VALUES(3,1, 3,20,0,100,'Installments');
INSERT INTO merchant_product(id,merchant_id,product_id,unit_price,discount_price,inventory,payment_option)
VALUES(4,1, 4,40,0,80,'Direct');
INSERT INTO merchant_product(id,merchant_id,product_id,unit_price,discount_price,inventory,payment_option)
VALUES(5,1, 5,50,0,60,'Installments');
INSERT INTO merchant_product(id,merchant_id,product_id,unit_price,discount_price,inventory,payment_option)
VALUES(6,1, 6,100,0,8,'Direct');
INSERT INTO merchant_product(id,merchant_id,product_id,unit_price,discount_price,inventory,payment_option)
VALUES(7,1,7,1500,0,4,'Direct');

