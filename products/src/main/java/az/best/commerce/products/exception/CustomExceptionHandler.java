package az.best.commerce.products.exception;

import az.best.commerce.products.dto.ServiceResponse;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.ArrayList;
import java.util.List;


@RestControllerAdvice
public class CustomExceptionHandler {
    private final Logger log = LogManager.getLogger(CustomExceptionHandler.class);

    @ExceptionHandler({ProductNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ServiceResponse hadnleUserNotException(ProductNotFoundException ex) {
        log.error(ex);
        return ServiceResponse.of(false,ex.getMessage(),null);
    }
}
