package az.best.commerce.products.dto;

import az.best.commerce.products.dto.Category;

public class ProductListResponseDto {
    private long id;
    private String name;
    private String destcription;
    private double unitPrice;
    private double discountPrice;
    private long inventory;
    private String paymentOption;
    private Category category;

    public ProductListResponseDto() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDestcription() {
        return destcription;
    }

    public void setDestcription(String destcription) {
        this.destcription = destcription;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public double getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(double discountPrice) {
        this.discountPrice = discountPrice;
    }

    public long getInventory() {
        return inventory;
    }

    public void setInventory(long inventory) {
        this.inventory = inventory;
    }

    public String getPaymentOption() {
        return paymentOption;
    }

    public void setPaymentOption(String paymentOption) {
        this.paymentOption = paymentOption;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "ProductListResponseDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", destcription='" + destcription + '\'' +
                ", unitPrice=" + unitPrice +
                ", discountPrice=" + discountPrice +
                ", inventory=" + inventory +
                ", paymentOption='" + paymentOption + '\'' +
                ", category=" + category +
                '}';
    }
}
