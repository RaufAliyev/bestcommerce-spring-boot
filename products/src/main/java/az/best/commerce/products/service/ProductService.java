package az.best.commerce.products.service;

import az.best.commerce.products.dto.Category;
import az.best.commerce.products.dto.ProductListResponseDto;
import az.best.commerce.products.entity.MerchantProduct;
import az.best.commerce.products.exception.ProductNotFoundException;
import az.best.commerce.products.repository.MerchantProductRepository;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class ProductService {

    private final Logger log = LogManager.getLogger(ProductService.class);

    private MerchantProductRepository repository;

    @Autowired
    public ProductService(MerchantProductRepository repository) {
        this.repository = repository;
    }

    public Map<String, Object> getProducts(long merchantId, int page, int size, String fieldName, String orderBy) {

        Sort sort = orderBy.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(fieldName).ascending() : Sort.by(fieldName).descending();
        Pageable pageAndSort = PageRequest.of(page, size, sort);

        Page<MerchantProduct> merchantProducts =  repository.findByMerchantId(merchantId, pageAndSort).orElseThrow(
                () -> new ProductNotFoundException("there is not any product")
        );

        List<ProductListResponseDto> responseDTOList = merchantProducts.getContent().stream().
                filter(merchantProduct -> merchantProduct.getInventory() > 5).
                   map(merchantProduct -> {
                    return getProductListResponseDto(merchantProduct);
                }).collect(Collectors.toList());

        Map<String, Object> response = getStringObjectMap(merchantProducts, responseDTOList);
        log.info(response);
        return response;
    }

    private Map<String, Object> getStringObjectMap(Page<MerchantProduct> merchantProducts, List<ProductListResponseDto> responseDTOList) {
        Map<String, Object> response = new HashMap<>();
        response.put("currentPage", merchantProducts.getNumber());
        response.put("totalItems", merchantProducts.getTotalElements());
        response.put("totalPages", merchantProducts.getTotalPages());
        response.put("products", responseDTOList);
        return response;
    }

    private ProductListResponseDto getProductListResponseDto(MerchantProduct merchantProduct) {
        ProductListResponseDto responseDto = new ProductListResponseDto();
        responseDto.setId(merchantProduct.getProduct().getId());
        responseDto.setName(merchantProduct.getProduct().getName());
        responseDto.setDestcription(merchantProduct.getProduct().getDescription());
        responseDto.setUnitPrice(merchantProduct.getUnitPrice());
        responseDto.setDiscountPrice(merchantProduct.getDiscountPrice());
        responseDto.setInventory(merchantProduct.getInventory());
        responseDto.setPaymentOption(merchantProduct.getPaymentOption());
        responseDto.setCategory(new Category(merchantProduct.getProduct().getCategory().getId(),
                merchantProduct.getProduct().getCategory().getName()));
        return responseDto;
    }

}