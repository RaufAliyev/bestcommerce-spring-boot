package az.best.commerce.products.exception;

public class CustomBaseException extends RuntimeException {

    public CustomBaseException(String message) {
        super(message);
    }

    public CustomBaseException(String message, Throwable cause) {
        super(message, cause);
    }
}