package az.best.commerce.products.repository;

import az.best.commerce.products.entity.MerchantProduct;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MerchantProductRepository extends JpaRepository<MerchantProduct, Long> {
    Optional<Page<MerchantProduct>> findByMerchantId(long id, Pageable pageable);
    Optional<List<MerchantProduct>> findByMerchantId(long id);
}

