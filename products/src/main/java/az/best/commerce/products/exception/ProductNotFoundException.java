package az.best.commerce.products.exception;

public class ProductNotFoundException extends CustomBaseException {
    public ProductNotFoundException(String message) {
        super(message);
    }
}
