package az.best.commerce.products.service;

import az.best.commerce.products.entity.Merchant;
import az.best.commerce.products.repository.MerchantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class MyUserDetailsService implements UserDetailsService {

    private MerchantRepository merchantRepository;

    @Autowired
    public MyUserDetailsService(MerchantRepository merchantRepository) {
        this.merchantRepository = merchantRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Merchant merchant = merchantRepository.findByEmailAddress(email).
                orElseThrow(() -> new UsernameNotFoundException("User Not Found with email: " + email));

        return new User(merchant.getEmailAddress(), merchant.getPassword(), new ArrayList<>());
    }
}
