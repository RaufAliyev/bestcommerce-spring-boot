package az.best.commerce.products.controller;

import az.best.commerce.products.dto.ServiceResponse;
import az.best.commerce.products.service.ProductService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Map;


@RestController
@RequestMapping(value = "/api/bestcommerce")
public class ProductController {

    private final Logger log = LogManager.getLogger(ProductController.class);

    private ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping(value = "/products")
    public ResponseEntity<ServiceResponse> product(@RequestParam(value = "merchantId",required = true) long merchantId,
                                                  @RequestParam(value = "page",defaultValue = "0") int page,
                                                  @RequestParam(value = "size",defaultValue = "3") int size,
                                                  @RequestParam(value = "fieldName", defaultValue = "id") String fieldName,
                                                  @RequestParam(value = "orderBy", defaultValue = "ASC")  String orderBy){
       log.info(String.format("[Request]  %s", merchantId,page,size,fieldName,orderBy));
       Map<String, Object>  products =  productService.getProducts(merchantId,page,size,fieldName,orderBy);
       ServiceResponse  serviceResponse = ServiceResponse.of(true,null,products);
       log.info(String.format("[Response]  %s",serviceResponse.toString()));
       return ResponseEntity.ok(serviceResponse);
    }
}
