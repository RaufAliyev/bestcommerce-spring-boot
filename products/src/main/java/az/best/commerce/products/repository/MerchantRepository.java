package az.best.commerce.products.repository;

import az.best.commerce.products.entity.Merchant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MerchantRepository extends JpaRepository<Merchant,Long> {
    Optional<Merchant> findByEmailAddress(String email);
}
