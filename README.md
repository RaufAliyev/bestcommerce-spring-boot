# BestCommerce-Spring Boot

BestCommerce, an e-commerce platform where merchants can sell their products online. It was written in Spring Boot. There are 3 separate project sign up, sign in and product. MySql was used as a shared database in projects.

## Sign up

 1. should change database url ,username and password
 2. spring.jpa.hibernate.ddl-auto = create-drop 
 3. when run project will create tables and will insert test data
 4. comment in application.properties spring.datasource.data=classpath:/category.sql,classpath:/product.sql
 5. spring.jpa.hibernate.ddl-auto = update
 
 **EndPoint**: /api/bestcommerce/sign-up <br />
 **Method** :  POST <br />
 **Request**: <br />
<pre>
		Body
{ 
	"address": "string", 
	"emailAddress": "string", 
	"name": "string", 
	"ownerName": "string",
	"password": "string", 
	"phoneNumber": "string", 
	"type": "string"
}			
</pre>
**Response**
HttpStatus: 201 Created

## Sign in

 1. should change database url ,username and password

**EndPoint**: /api/bestcommerce/sign-in <br />
**Method** : POST <br />
**Request**: <br />
<pre>
        Body
{ 
    "email":"string",
    "password": "string" 
}
  </pre>

	
## Product

 - should change database url ,username and password
 - run project
 - comment in application.properties spring.datasource.data=classpath:/mp.sql

**EndPoint**: /api/bestcommerce/products <br />
**Method** : GET <br />
**Header**: <br />
   <pre>
      Authorization: Bearer "jwt"
  </pre>
  
**Request Params**: 

 - merchantId 
 - page  
 - size 
 - fieldName  
 - orderBy (asc/desc)

