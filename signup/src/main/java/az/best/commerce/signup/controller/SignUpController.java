package az.best.commerce.signup.controller;

import az.best.commerce.signup.dto.MerchantRequestDto;
import az.best.commerce.signup.service.SignUpService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/bestcommerce")
public class SignUpController {
    private final Logger log = LogManager.getLogger(SignUpController.class);

    private SignUpService signUpService;

    @Autowired
    public SignUpController(SignUpService signUpService) {
        this.signUpService = signUpService;
    }

    @PostMapping(value = "/sign-up")
    public ResponseEntity addMerchant(@Valid @RequestBody MerchantRequestDto requestDto){
        log.info(String.format("[Request]  %s", requestDto.toString()));
        signUpService.saveMerchant(requestDto);
        log.info(String.format("[Response]  %s", "Success"));
        return new ResponseEntity(HttpStatus.CREATED);
    }
}
