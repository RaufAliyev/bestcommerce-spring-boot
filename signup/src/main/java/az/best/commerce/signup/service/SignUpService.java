package az.best.commerce.signup.service;

import az.best.commerce.signup.dto.MerchantRequestDto;
import az.best.commerce.signup.entity.Merchant;
import az.best.commerce.signup.repositroy.SignUpRepository;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class SignUpService {
    private final Logger log = LogManager.getLogger(SignUpService.class);

    private SignUpRepository sessionRepository;

    @Autowired
    public SignUpService(SignUpRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }

    public void saveMerchant(MerchantRequestDto merchant){
        merchant.setPassword(encodePassword(merchant.getPassword()));
        ModelMapper mm = new ModelMapper();
        Merchant merchantEntity = mm.map(merchant, Merchant.class);
        log.info(merchantEntity.toString());
        sessionRepository.save(merchantEntity);
    }

    private String encodePassword(String password){
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String hashedPassword = passwordEncoder.encode(password);
        return hashedPassword;
    }
}
