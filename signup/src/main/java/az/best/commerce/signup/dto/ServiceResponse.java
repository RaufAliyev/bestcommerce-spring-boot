package az.best.commerce.signup.dto;

public class ServiceResponse {
        private boolean success;
        private String message;
        private Object payload;

        public static ServiceResponse of(boolean success, String message, Object payload) {
            ServiceResponse serviceResponse = new ServiceResponse();
            serviceResponse.setSuccess(success);
            serviceResponse.setMessage(message);
            serviceResponse.setPayload(payload);
            return serviceResponse;
        }

    public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Object getPayload() {
            return payload;
        }

        public void setPayload(Object payload) {
            this.payload = payload;
        }

        @Override
        public String toString() {
            return "ServiceResponse{" +
                    "success=" + success +
                    ", message='" + message + '\'' +
                    ", payload=" + payload +
                    '}';
        }
}
