package az.best.commerce.signup.repositroy;

import az.best.commerce.signup.entity.Merchant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SignUpRepository extends JpaRepository<Merchant, Long> {
}
